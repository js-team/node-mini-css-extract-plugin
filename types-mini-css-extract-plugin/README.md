# Installation
> `npm install --save @types/mini-css-extract-plugin`

# Summary
This package contains type definitions for mini-css-extract-plugin (https://github.com/webpack-contrib/mini-css-extract-plugin).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/mini-css-extract-plugin.

### Additional Details
 * Last updated: Sun, 17 Oct 2021 07:31:17 GMT
 * Dependencies: [@types/webpack](https://npmjs.com/package/@types/webpack), [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [JounQin](https://github.com/JounQin), [Katsuya Hino](https://github.com/dobogo), [Spencer Miskoviak](https://github.com/skovy), [Piotr Błażejewicz](https://github.com/peterblazejewicz), and [James Garbutt](https://github.com/43081j).
